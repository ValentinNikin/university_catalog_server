﻿using System.Collections.Generic;

namespace Catalog.core.Entities
{
    public class CategoryNode
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ImagePath { get; set; }
        public int ParentCategoryId { get; set; }
        public IEnumerable<CategoryNode> ChildCategories { get; set; }
    }
}

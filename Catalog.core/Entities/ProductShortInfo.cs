﻿namespace Catalog.core.Entities
{
    public class ProductShortInfo
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ImagePath { get; set; }
        public int CategoryId { get; set; }
        public decimal Price { get; set; }
    }
}

﻿using System;

namespace Catalog.core.Entities
{
    public class ProductInfo
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
        public int CategoryId { get; set; }
        public decimal Price { get; set; }
        public string Maker { get; set; }
        public string MakeDate { get; set; }
    }
}

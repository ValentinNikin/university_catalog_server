﻿namespace Catalog.core.Entities
{
    public class CategoryInfo
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool ProductsCategory { get; set; }
        public string ImagePath { get; set; }
    }
}

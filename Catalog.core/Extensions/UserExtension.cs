﻿using Catalog.core.Entities;
using Catalog.core.Helpers;
using System;

namespace Catalog.core.Extensions
{
    public static class UserExtension
    {
        public static bool VerifyPasswordHash(this User user, string password)
        {
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }
            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentException("Value can't be empty, or whitespace", "password");
            }
            if (user.PasswordHash.Length != 64)
            {
                throw new ArgumentException("Invalid length of password hash (64 bytes expected)", "passwordHash");
            }
            if (user.PasswordSalt.Length != 128)
            {
                throw new ArgumentException("Invalid length of password salt (128 bytes expected)", "passwordHash");
            }

            return CryptographyHelper.CheckPassword(password, user.PasswordSalt, user.PasswordHash);
        }

        public static void CreatePasswordHash(this User user, string password)
        {
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }
            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentException("Value can't be empty, or whitespace", "password");
            }

            CryptographyHelper.CreateHash(password, out byte[] salt, out byte[] hash);

            user.PasswordSalt = salt;
            user.PasswordHash = hash;
        }
    }
}

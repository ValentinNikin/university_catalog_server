﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Catalog.core.Helpers
{
    public static class CryptographyHelper
    {
        public static bool CheckPassword(string password, byte[] salt, byte[] hash)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(salt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (var i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != hash[i])
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public static void CreateHash(string password, out byte[] salt, out byte[] hash)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                salt = hmac.Key;
                hash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
    }
}

﻿using AutoMapper;
using Catalog.core.Entities;
using Catalog.infrastructure.Database.Models;
using Catalog.Models;

namespace Catalog.Helpers
{
    public class AutoMapper : Profile
    {
        public AutoMapper()
        {
            CreateMap<RegisterModel, User>();
            CreateMap<User, AuthorizedUserModel>();
            CreateMap<CategoryDB, CategoryInfo>()
                .ForMember(d => d.ProductsCategory, m => m.MapFrom(c => c.Categories.Count == 0));
            CreateMap<CategoryDB, CategoryNode>()
                .ForMember(d => d.ParentCategoryId, m => m.MapFrom(c => c.ParentCategory != null ? c.ParentCategory.Id : -1));
            CreateMap<ProductDB, ProductInfo>()
                .ForMember(d => d.CategoryId, m => m.MapFrom(c => c.Category.Id))
                .ForMember(d => d.MakeDate, m => m.MapFrom(c => c.MakeDate.ToString()));
            CreateMap<ProductDB, ProductShortInfo>()
                .ForMember(d => d.CategoryId, m => m.MapFrom(c => c.Category.Id));
        }
    }
}
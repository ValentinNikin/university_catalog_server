﻿using System.ComponentModel.DataAnnotations;

namespace Catalog.Models
{
    public class AuthenticationModel
    {
        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }
    }
}

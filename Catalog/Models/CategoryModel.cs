﻿using Catalog.core.Entities;
using System.Collections.Generic;

namespace Catalog.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public byte[] ImageData { get; set; }
        //public List<ShopNode> Children { get; set; }
    }
}

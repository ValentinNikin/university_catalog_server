﻿namespace Catalog.Models
{
    public class AuthorizedUserModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public string JwtToken { get; set; }
    }
}

﻿using Catalog.infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Catalog.Controllers
{
    [Route("api/catalog")]
    [ApiController]
    public class CatalogController : Controller
    {
        private ICatalogService catalog;

        public CatalogController(ICatalogService catalog)
        {
            this.catalog = catalog;
        }

        /// <summary>
        /// Получение списка всех продуктов
        /// </summary>
        /// <returns></returns>
        [HttpGet("allProducts")]
        public JsonResult GetAll()
        {
            var products = this.catalog.GetAllProducts();
            return Json(products);
        }

        /// <summary>
        /// Получение списка всех категорий
        /// </summary>
        /// <returns></returns>
        [HttpGet("categories")]
        public JsonResult GetCategories()
        {
            var categories = this.catalog.GetCategoriesTree();
            return Json(categories);
        }

        /// <summary>
        /// Получение списка основных категорий
        /// </summary>
        /// <returns></returns>
        [HttpGet("mainCategories")]
        public JsonResult GetMainCategories()
        {
            var mainCategories = this.catalog.GetMainCategories();
            return Json(mainCategories);
        }

        /// <summary>
        /// Получение детальной информации о категории
        /// </summary>
        /// <param name="id">id целевой категории</param>
        /// <returns></returns>
        [HttpGet("categories/{id}")] 
        public JsonResult GetCategoryInfo(int id)
        {
            var category = this.catalog.GetCategoryById(id);
            return Json(category);
        }

        /// <summary>
        /// Получить дочерние категории
        /// </summary>
        /// <param name="id">id категории для которой нужно получить список дочерних</param>
        /// <returns></returns>
        [HttpGet("childCategories/{id}")]
        public JsonResult GetChildCategories(int id)
        {
            var categories = this.catalog.GetChildCategories(id);
            return Json(categories);
        }

        /// <summary>
        /// Получить список продуктов для заданной категории
        /// </summary>
        /// <param name="id">id категории для которой нужно получить список продуктов</param>
        /// <returns></returns>
        [HttpGet("productsFromCategory/{id}")]
        public JsonResult GetProductsByCategory(int id)
        {
            var products = this.catalog.GetProductsByCategory(id);
            return Json(products);
        }

        /// <summary>
        /// Получить информацию о продукте по id
        /// </summary>
        /// <param name="id">id целевого продукта</param>
        /// <returns></returns>
        [HttpGet("products/{id}")]
        public JsonResult GetProductById(int id)
        {
            var product = this.catalog.GetProductById(id);
            return Json(product);
        }
    }
}
﻿using Catalog.core.Entities;
using Catalog.core.Exceptions;
using Catalog.core.Extensions;
using Catalog.infrastructure.Database.Models;
using Catalog.infrastructure.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Catalog.infrastructure.Services
{
    public class UserService : IUserService
    {
        private List<User> users = new List<User>();

        public UserService() { }

        public User Authenticate(string login, string password)
        {
            if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
            {
                return null;
            }

            var user = users.SingleOrDefault(x => x.Login == login);

            if (user == null)
            {
                return null;
            }

            if (!user.VerifyPasswordHash(password))
            {
                return null;
            }

            return user;
        }

        public User Create(User user, string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                throw new AppException("Password is required");
            }

            if (users.Any(u => u.Login == user.Login))
            {
                throw new AppException($"User with login {user.Login} already exist");
            }

            user.CreatePasswordHash(password);
            user.Id = users.Count;

            users.Add(user);

            return user;
        }

        public User GetById(int userId)
        {
            var user = users.FirstOrDefault(u => u.Id == userId);

            return user;
        }
    }
}

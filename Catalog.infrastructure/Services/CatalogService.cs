﻿using AutoMapper;
using Catalog.core.Entities;
using Catalog.infrastructure.Database;
using Catalog.infrastructure.Database.Models;
using Catalog.infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Catalog.infrastructure.Services
{
    public class CatalogService : ICatalogService
    {
        private CatalogDbContext context = null;
        private IMapper mapper = null;

        public CatalogService(
            CatalogDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public IEnumerable<ProductInfo> GetAllProducts()
        {
            return this.context.Products.Include(p => p.Category).Select(p => mapper.Map<ProductInfo>(p)).ToList();
        }

        public IEnumerable<CategoryNode> GetCategoriesTree()
        {
            var categories = this.context.Categories.ToList();
            return BuildTreeCategories(categories);
        }

        public IEnumerable<CategoryNode> GetMainCategories()
        {
            return this.context.Categories
                .Where(c => c.ParentCategory == null)
                .Select(c => mapper.Map<CategoryNode>(c));
        }

        public CategoryInfo GetCategoryById(int id)
        {
            return this.context.Categories
                .Include(c => c.Categories)
                .Where(c => c.Id == id)
                .Select(c => mapper.Map<CategoryInfo>(c))
                .FirstOrDefault();
        }

        public IEnumerable<CategoryInfo> GetChildCategories(int id)
        {
            return this.context.Categories
                .Where(c => c.ParentCategory.Id == id)
                .Select(c => mapper.Map<CategoryInfo>(c));
        }

        public IEnumerable<ProductShortInfo> GetProductsByCategory(int id)
        {
            return this.context.Products
                .Include(p => p.Category)
                .Where(p => p.Category.Id == id)
                .Select(p => mapper.Map<ProductShortInfo>(p));
        }
        
        public ProductInfo GetProductById(int id)
        {
            return this.context.Products
                .Where(p => p.Id == id)
                .Select(p => mapper.Map<ProductInfo>(p))
                .FirstOrDefault();
        }


        private IEnumerable<CategoryNode> BuildTreeCategories(IEnumerable<CategoryDB> categories, CategoryDB parentCategory = null)
        {
            return categories?.Where(c => c.ParentCategory == parentCategory).Select(c => new CategoryNode
            {
                Id = c.Id,
                Title = c.Title,
                ImagePath = c.ImagePath,
                ParentCategoryId = c.ParentCategory != null ? c.ParentCategory.Id : -1,
                ChildCategories = BuildTreeCategories(categories, c)
            });
        }
    }
}

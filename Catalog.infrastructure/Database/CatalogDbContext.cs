﻿using Catalog.infrastructure.Database.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Catalog.infrastructure.Database
{
    public class CatalogDbContext : DbContext
    {
        public DbSet<CategoryDB> Categories { get; set; }
        public DbSet<ProductDB> Products { get; set; }

        public DbSet<UserDB> Users { get; set; }

        public CatalogDbContext(DbContextOptions<CatalogDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}

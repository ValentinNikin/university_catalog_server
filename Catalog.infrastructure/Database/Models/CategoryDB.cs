﻿using System.Collections.Generic;

namespace Catalog.infrastructure.Database.Models
{
    public class CategoryDB : ShopNodeDB
    {
        public CategoryDB ParentCategory { get; set; }
        public List<CategoryDB> Categories { get; set; }
        public List<ProductDB> Products { get; set; }

        public CategoryDB(string title) : base(title, ShopNodeType.Category) { }
    }
}

﻿namespace Catalog.infrastructure.Database.Models
{
    public enum ShopNodeType
    {
        Undefined = 0,
        Category,
        Product
    }

    public abstract class ShopNodeDB
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImagePath { get; set; }
        public ShopNodeType Type { get; set; }

        public ShopNodeDB(string title, ShopNodeType type)
        {
            this.Title = title;
            this.Type = type;
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Catalog.infrastructure.Database.Models
{
    public class ProductDB : ShopNodeDB
    {
        public CategoryDB Category { get; set; }
        public decimal Price { get; set; }
        public string Maker { get; set; }
        public DateTime MakeDate { get; set; }

        public ProductDB(string title) : base(title, ShopNodeType.Product) { }
    }
}

﻿using Catalog.core.Entities;
using System.Collections.Generic;

namespace Catalog.infrastructure.Interfaces
{
    public interface ICatalogService
    {
        IEnumerable<ProductInfo> GetAllProducts();
        IEnumerable<CategoryNode> GetCategoriesTree();
        IEnumerable<CategoryNode> GetMainCategories();
        CategoryInfo GetCategoryById(int id);
        IEnumerable<CategoryInfo> GetChildCategories(int id);
        IEnumerable<ProductShortInfo> GetProductsByCategory(int id);
        ProductInfo GetProductById(int id);
    }
}

﻿using Catalog.core.Entities;

namespace Catalog.infrastructure.Interfaces
{
    public interface IUserService
    {
        User Authenticate(string login, string password);
        User Create(User user, string password);
        User GetById(int userId);
    }
}
